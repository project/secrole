Secure by Role
--------------

Project Page:
http://drupal.org/project/secrole

By Garrett Albright
http://drupal.org/user/191212
Email: albright (at) anre [dot] net
AIM: AlbrightGuy - ICQ: 150670553 - IRC: Albright


Caveats - READ THIS!
--------------------

MERELY INSTALLING THIS MODULE IS NOT ENOUGH TO SECURE YOUR SITE. Further
configuration is necessary. Read the instructions below closely.

THIS MODULE AND/OR YOUR SERVER CAN BE MISCONFIGURED IN A WAY WHICH WILL CAUSE
YOU TO LOSE ALL ACCESS TO YOUR SITE. See the "Oh noes, my site is hosed!"
section below. If you don't understand the instructions in that section, please
ask someone more experienced to help you install this module. I personally will
be glad to help you if you contact me - but probably not for free, especially if
you can't at least handle step 1 in the installation instructions below by
yourself.


About Secure by Role
--------------------

Secure by Role is a simple Drupal module for shopping sites and others where
security is important. When a page is requested, SecRole checks to see if the
user is a member of a list of roles which should be only served pages over a
secure connection (HTTPS). If this is the case, and the connection is not
already secure, the user is redirected to the page they are requesting over a
secure connection. It can also assure that log in forms are always served over a
secure connection, so that a site's users' passwords are never sent over an
insecure connection while they are logging in.

Why is this module's approach better than simply locking down your entire site
behind a secure connection? Because not all the visitors to your site will be
doing things which demand a secure connection. Search engine spiders, for
example, are not going to be sending credit card details; they may even be
incapable of indexing pages behind HTTPS connections. Or maybe they're simply
human users who are just browsing with no intent to buy anything - yet. But with
SecRole, you can easily configure the site so that, once these users create an
account, they are served over a secure connection and their credit card details
are safe. Your web server can avoid the overhead incurred by encrypting and
decrypting data sent or received over a secure connection until it's necessary.

SecRole has successfully been tested with both Apache and Lighttpd web server
software. (If you've tested SecRole on another server, please let me know your
results!) SecRole expects HTTPS connections to be served over port 443; if your
web server is configured to use a different port, you will have to adapt
SecRole's code before it will work properly for you. (Or you could just stop
using the wrong ports for things, please.)


Installation & Configuration
----------------------------

1. Configure an HTTPS connection for your site. How to do this is beyond the
scope of this module's documentation. If your site is hosted by a shared hosting
company, contact them for more information; most likely, you'll need to pay a
significant additional fee for this service. If you have your own dedicated
server or VPS, consult the documentation for the web server program you are
using. There's no point in continuing to step 2 until your site is accessible
over an HTTPS connection.

2. Install the Secure by Role module just like any other module. YOUR SITE IS
NOT SECURE YET. KEEP READING.

3. Configure the module by heading to Administer > Site configuration > Secure
by Role. Here's a run-down of the options on this page:

  Enable Secure by Role: You have three options: "On (Secure mode)," "On (Test
  mode)," and "Off." Note that by default SecRole is off. The crucial
  difference between "Secure mode" and "Test mode" is that the latter can be
  easily disabled if something goes wrong while testing; see the "Oh noes"
  section below. Note that none of the other settings on this page will matter
  if this is set to "Off."
  
  Secure Server URL: If your web server is configured so that the HTTPS
  connection is served over a different domain name or no domain name at all
  (just an IP address), enter the web address to the server via an HTTPS
  connection here. If this field is left blank, then by default, SecRole will
  just use whatever the current domain name is. For example, if
  http://example.com/foo is requested and SecRole decides it should be sent
  over a secure connection, the user is redirected to https://example.com/foo.
  
  Enabled roles: Select which roles should be served secure pages. For most
  sites, it is probably enough to simply set the "anonymous user" role unchecked
  and the "authenticated user" checked. (Remember that all users who log in will
  be a member of the "authenticated user" group, even if they're a member of
  other groups as well.)
  
  Secure the login page: For an extra dash of security, check this box. SecRole
  will make sure that any page which the user could possibly log in from is
  sent over a secure connection. But be warned that this may produce unwanted
  results if you have the login block on all or most of the pages on your site!
  
Click the "Save configuration" button at the bottom to save your changes. Note
that if you enabled SecRole for members with the "authenticated user" group as
recommended above, then as soon as you click this button, you'll be redirected
to a secure connection, so if something about your configuration doesn't work,
you'll find out quick!


Oh noes, my site is hosed!
--------------------------

If Secure by Role is in testing mode and you are unable to access the site,
it's very easy to disable SecRole and get your site back to normal. Simply add
"?secrole=off" to the URL of any page on your site - though for best results,
you may also have to alter the protocol part of the URL so that it's no longer
trying to access the HTTPS server. For example, if you're trying to get to
https://example.com/admin, try instead http://example.com/admin?secrole=off.
Note that if you have clean URLs disabled, you'll want to change that question
mark to an ampersand, so that http://example.com/?q=admin becomes
http://example.com?q=adim&secrole=off.

If the above doesn't work, or you're locked out of your site while it's in
"Secure mode," you can recover your site by running some SQL commands against
your site's database via the command line or a web-based interface such as
phpMySQL. Run the following two SQL lines (you'll need to tweak them if your
database tables use prefixes):

UPDATE variable SET value = 's:1:"0";' WHERE name = 'secrole_on';
DELETE FROM cache WHERE cid = 'variables';

Running these lines is basically the equivalent of setting the "Enable SecRole"
switch to "off." After running these lines, you should be able to access your
site again. Adjust your configuration and try again.

If you don't have access to the SQL database… Wow, you really hosed yourself.
Don't give up hope yet, though. You can regain access to your site by editing
SecRole's code as a last resort. Go into the secrole.module code file and delete
or comment out the line which looks like this (it's line 47 as I write this,
but it may wander as development continues on this module):

        _secrole_direct();

That will stop SecRole from kicking in long enough for you to go in and disable
the module and then ask someone to help you with it.